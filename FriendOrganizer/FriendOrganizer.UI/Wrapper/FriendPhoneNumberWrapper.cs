﻿namespace FriendOrganizer.UI.Wrapper
{
    using FriendOrganizer.Model;

    public class FriendPhoneNumberWrapper : ModelWrapper<FriendPhoneNumber>
    {
        public FriendPhoneNumberWrapper(FriendPhoneNumber model)
            : base(model)
        {
        }

        public string Number
        {
            get => GetValue<string>();
            set => SetValue(value);
        }
    }
}