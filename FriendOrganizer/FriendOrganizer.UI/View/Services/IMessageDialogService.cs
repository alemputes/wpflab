﻿namespace FriendOrganizer.UI.View.Services
{
    using System.Threading.Tasks;

    public interface IMessageDialogService
    {
        Task ShowInfoDialogAsync(string text);

        Task<MessageDialogResult> ShowOkCancelDialogAsync(string text, string title);
    }
}