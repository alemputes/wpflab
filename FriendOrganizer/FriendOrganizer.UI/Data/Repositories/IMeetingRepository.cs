﻿namespace FriendOrganizer.UI.Data.Repositories
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using FriendOrganizer.Model;

    public interface IMeetingRepository : IGenericRepository<Meeting>
    {
        Task<List<Friend>> GetAllFriendsAsync();

        Task ReloadFriendAsync(int friendId);
    }
}