﻿namespace FriendOrganizer.UI.Data.Repositories
{
    using System.Threading.Tasks;

    using FriendOrganizer.Model;

    public interface IProgrammingLanguageRepository : IGenericRepository<ProgrammingLanguage>
    {
        Task<bool> IsReferencedByFriendAsync(int programmingLanguageId);
    }
}