﻿namespace FriendOrganizer.UI.Data.Repositories
{
    using System.Threading.Tasks;

    using FriendOrganizer.Model;

    public interface IFriendRepository : IGenericRepository<Friend>
    {
        Task<bool> HasMeetingsAsync(int friendId);

        void RemovePhoneNumber(FriendPhoneNumber friendPhoneNumberModel);
    }
}