﻿namespace FriendOrganizer.UI
{
    using System.Windows;
    using FriendOrganizer.UI.ViewModel;

    using MahApps.Metro.Controls;

    public partial class MainWindow : MetroWindow
    {
        private readonly MainViewModel _mainViewModel;

        public MainWindow(MainViewModel mainViewModel)
        {
            InitializeComponent();
            _mainViewModel = mainViewModel;
            DataContext = _mainViewModel;
            Loaded += OnLoaded;
        }

        private async void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            await _mainViewModel.LoadAsync();
        }
    }
}