﻿namespace FriendOrganizer.UI.ViewModel
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    using FriendOrganizer.UI.Properties;

    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
