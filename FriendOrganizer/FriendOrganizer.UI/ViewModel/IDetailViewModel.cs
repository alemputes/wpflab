﻿namespace FriendOrganizer.UI.ViewModel
{
    using System.Threading.Tasks;

    public interface IDetailViewModel
    {
        bool HasChanges { get; }

        int Id { get; }

        Task LoadAsync(int id);
    }
}