﻿namespace FriendOrganizer.Model
{
    using System.ComponentModel.DataAnnotations;

    public class ProgrammingLanguage
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
    }
}